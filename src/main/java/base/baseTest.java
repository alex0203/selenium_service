package base;

import org.junit.Before;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class baseTest {

    public WebDriver driver;

    @Before
    public void tearUp() throws MalformedURLException {
        ChromeOptions capabilities = new ChromeOptions();
        driver = new RemoteWebDriver(new URL("https://localhost:4444/wd/hub"), capabilities);
    }
}
